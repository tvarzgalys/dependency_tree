import os
import ast
from graphviz import Digraph

DependencyTree = Digraph(comment='DependencyTree')
help = "****************\n" \
       "Common commands:\n\n" \
       " [help]           Shows this usage\n" \
       " [config]         Get path from odoo-server.conf, use with\n" \
       "                  [-one][-all], than specify path of config file\n" \
       " [dir]            Use with [-one][-all] before specify directory, \n" \
       " [--one]          Draw one module dependency until base\n" \
       " [--all]          (default) Draw all modules found in directory\n\n" \
       " Example: dir --one /home/modules/project_pass\n\n" \
       "****************\n"
print help
sourceDir = ''
waitingCommand = ('//: ')
command = raw_input(waitingCommand)
correctSyntax = False


modules = []

names = []
dependencies = []
root = ''


def get_dependencies_by_name(moduleName):
    return dependencies[names.index(moduleName)]


def get_name_by_dependencies(dependecies):
    return names[dependencies.index(dependecies)]


def read_dependencies():
    modules = os.listdir(sourceDir)
    for name in modules:
        path = (sourceDir + '/' + name + '/__openerp__.py')
        if os.path.exists(path):
            openerpFile = open(path, 'r')
            cleanOpenerpFile = ''
            for line in openerpFile:
                if "#" not in line and line != '\n':
                    cleanOpenerpFile += line
            OpenerpFileDict = ast.literal_eval(cleanOpenerpFile)
            names.append(name)
            dependencies.append(OpenerpFileDict['depends'])


def draw_dependencies_until_base():
    dependsOn = [root] + get_dependencies_by_name(root)
    DependencyTree.node(root, shape='house')
    for dependSet in dependsOn:
        try:
            for element in get_dependencies_by_name(dependSet):
                if element not in dependsOn:
                    dependsOn.append(element)
            for dependency in get_dependencies_by_name(dependSet):
                if dependSet != dependency:
                    # DependencyTree.node(dependency, shape='box')
                    if dependency not in modules:
                        DependencyTree.node(dependency, shape='rectangle')
                    DependencyTree.edge(dependSet, dependency)
        except ValueError:
            pass
    DependencyTree.render('/home/tomas/Desktop/graphvikz/tree.gv', view=True)


def draw_all_dependencies():
    drawed = [[], []]
    for dependencySet in dependencies:
        for element in dependencySet:
            name = get_name_by_dependencies(dependencySet)
            if name not in drawed[0] or element not in drawed[1]:
                drawed[0].append(name)
                drawed[1].append(element)
                DependencyTree.edge(get_name_by_dependencies(dependencySet),
                                    element)
    DependencyTree.render('/home/tomas/Desktop/graphvikz/tree.gv', view=True)



while (correctSyntax == False):
    if command == 'help':
        print help
        command = raw_input(waitingCommand)
    elif command == 'config':
        sourceDir = ''
        correctSyntax = True
    elif command == 'config --one':
        sourceDir = ''
        correctSyntax = True
    elif command == 'config --all':
        sourceDir = ''
        correctSyntax = True
    elif command[:3] == 'dir':
        if command[:9] == 'dir --one':
            sourceDir = os.path.abspath(os.path.join(command[10:], os.pardir))
            root = os.path.basename(os.path.normpath(command[10:]))
            try:
                read_dependencies()
                draw_dependencies_until_base()
            except Exception:
                print ('There is no such directory: ' + sourceDir + '\n//: ')
                pass
            correctSyntax = True
        elif command[:9] == 'dir --all':
            sourceDir = command[10:]
            try:
                draw_all_dependencies()
            except ValueError:
                print ('There is no such directory: ' + sourceDir + '\n//: ')
                pass
            correctSyntax = True
        else:
            print 'else'
            sourceDir = command[6:]
            try:
                draw_all_dependencies()
            except ValueError:
                print ('There is no such directory: ' + sourceDir + '\n//: ')
                pass
            correctSyntax = True

    else:
        command = raw_input(
            'Wrong syntax! Type --help for available commands.\n//: ')


# if __name__ == '__main__':
#     # read_dependencies()
#     # draw_dependencies_until_base()